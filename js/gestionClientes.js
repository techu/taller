var clientesObtenidos;

function getClientes() {

  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";

  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // Typical action to be performed when the document is ready:
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  };
  request.open("GET", url, true);
  request.send();
}

function procesarClientes() {
  var jsonClientes = JSON.parse(clientesObtenidos);

  var tabla = document.getElementById("tablaClientes");

  var body = document.createElement("tbody");
  tabla.appendChild(body);

  for (var i=0; i < jsonClientes.value.length; i++) {

    console.log("name=" + jsonClientes.value[i].ContactName
    + " direccion=" + jsonClientes.value[i].Address
    + " ciudad=" + jsonClientes.value[i].City
    + " pais=" + jsonClientes.value[i].Country);

    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = jsonClientes.value[i].ContactName;

    var columnaDireccion = document.createElement("td");
    columnaDireccion.innerText = jsonClientes.value[i].Address;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = jsonClientes.value[i].City;

    var columnaPais = document.createElement("td");
    // columnaPais.innerText = jsonClientes.value[i].Country;

    var imagen = document.createElement("img");
    // imagen.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-Spain.png";

    var country = jsonClientes.value[i].Country;
    if (country == 'UK') {
      country = 'United-Kingdom';
    }

    imagen.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + country + ".png";
    // imagen.style.width = '50px';
    // imagen.style.height = '25px';
    imagen.classList.add("flag");
    columnaPais.appendChild(imagen);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaDireccion);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaPais);

    body.appendChild(nuevaFila);

  }
}
