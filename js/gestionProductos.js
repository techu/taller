var productosObtenidos;

function getProductos() {

  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Products";

  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // Typical action to be performed when the document is ready:
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  };
  request.open("GET", url, true);
  request.send();
}

function procesarProductos() {
    var jsonProductos = JSON.parse(productosObtenidos);

    var tabla = document.getElementById("tablaProductos");

    var body = document.createElement("tbody");
    tabla.appendChild(body);

    for (var i=0; i < jsonProductos.value.length; i++) {

      console.log("producto=" + jsonProductos.value[i].ProductName
      + " precio=" + jsonProductos.value[i].UnitPrice
      + " stock=" + jsonProductos.value[i].UnitsInStock);

      var nuevaFila = document.createElement("tr");

      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = jsonProductos.value[i].ProductName;

      var columnaPrecio = document.createElement("td");
      columnaPrecio.innerText = jsonProductos.value[i].UnitPrice;

      var columnaStock = document.createElement("td");
      columnaStock.innerText = jsonProductos.value[i].UnitsInStock;

      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaPrecio);
      nuevaFila.appendChild(columnaStock);

      body.appendChild(nuevaFila);

    }
}
